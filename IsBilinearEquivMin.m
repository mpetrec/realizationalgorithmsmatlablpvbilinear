%This function returns 1 if  the bilinear system (Cnum1,A1,...,AD,x01) and
%(Cnum2,B1,...,BD,x02) realize the same input-output map, where
% Anum1=[A1;A2;..;AD], Anum2=[B1;B2;...;BD]. It also returns an
% isomoprhism T if (Cnum2,B1,...,BD,x02) is observable and has the same
% dimension as (Cnum1,A1,...,AD,x01). If both systems are minimal, then T
% is an isomorphism from (Cnum1,A1,...,AD,x01) to (Cnum2,B1,...,BD,x02)

function [isEquiv,T] = IsBilinearEquivFast(Anum1,Cnum1,x01, Anum2,Cnum2,x02)

epsilon = 1e-5;

n = size(Anum1,2);
D = size(Anum1,1)/n;
n2 =size(Anum2,2);

% if (size(Anum1,2) ~= size(Anum2,2))
%      isEquiv = 0;
%      T = zeros(size(Anum1,2),size(Anum1,2));
%      return
% end

Anum3 = []

for i=1:D
    Anum3=[Anum3;blkdiag(Anum1((i-1)*n+1:i*n,:),Anum2((i-1)*n2+1:i*n2,:))];
end

x03 = [x01;x02];
    
Cnum3 = [Cnum1,Cnum2];


[Wf,Ao,Co,xo] = ObsRedBil(Anum3,Cnum3,x03);

S = Wf'

isEquiv = (norm(S*[x01;-x02],2) < epsilon)

if (n==n2 & IsMinBil(Anum1,Cnum1,x01) & IsMinBil(Anum2,Cnum2,x02) & isEquiv)

  T1 = S(:,1:n);
  T2 = S(:,n+1:end)
    
  T=T1*inv(T2);
 
else
     T = zeros(n,n);
end    
 
  T*x01-x02
  kron(T, eye(D))*Anum1-Anum2*T
  Cnum1-Cnum2*T
end

