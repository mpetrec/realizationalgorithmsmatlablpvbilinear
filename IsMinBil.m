%This function checks if  a bilinear system (C,A_1,...,A_D,x_0) 
%system is minimal.
% Anum=[A1;A2;..;AD], C 
% x0 -- initial state
%The function returns true if the bilinear system is minimal and false
%otherwise.

function [IsMin]=IsMinBil(Anum,C,x0)
  IsMin = (IsObsBil(Anum,C,x0) & IsReachBil(Anum,C,x0));
end