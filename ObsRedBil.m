%This function performs observability reduction of a bilinear system (C,A_1,..,A_D,x_0).
% Anum=[A1;A2;..;AD], C 
% x0 -- initial state
%The
%resulting transformation matrix is stored in Wf and 
%the matrices of the reduced system are stored in Ao,Bo,Co,xo0, according
%to the same principle as the input is stored; red_ord_o is the order of
%the reduced system.

function [Wf,red_ord_o,Ao,Co,xo0]=ObsRedBil(Anum,C,x0)


   n = size(Anum,2);
   D = size(Anum,1)/n;
   p = size(C,1);
   

    Wf=orth(C');

    
    quit = 0;
    r=rank(Wf);
    while (quit == 0)
       Wprime=C';
       r = rank(Wf);
       for j=1:D
          Wprime=[Wprime, Anum((j-1)*n+1:j*n,:)'*Wf];
       end
       Wf=orth(Wprime);
       quit = (r == rank(Wf));    
    end
    
    red_ord_o=r;
    Ao=zeros(D*red_ord_o,red_ord_o);
    
        
    xo0=Wf'*x0;
   
    Co=C*Wf;
    
    for q=1:D
        Ao((q-1)*red_ord_o+1:q*red_ord_o,:)=Wf'*Anum((q-1)*n+1:q*n,:)*Wf;
    end
    
    
end