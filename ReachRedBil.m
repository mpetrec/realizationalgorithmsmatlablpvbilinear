%This function performs reachability reduction of a bilinear systme (C,A_1,...,A_D,x_0) 
%system. 
% Anum=[A1;A2;..;AD], C 
% x0 -- initial state
%The
%resulting transformation matrix is stored in Reach_mat and 
%the matrices of the reduced system are stored in Ar,Br,Cr,x0r, according
%to the same principle as the input is stored; red_ord_r is the order of
%the reduced system.

function [Reach_mat,red_ord_r,Ar,Cr,x0r]=ReachRedBil(Anum,C,x_0)

     n = size(Anum,2);
     D = size(Anum,1)/n;
     p = size(C,1);
   
      V_f=orth(x_0);
      V_0=V_f;

      quit = 0;
      r=rank(V_f);
      while (quit == 0)
          r=rank(V_f);
          V_prime = V_0;
          for j=1:D
              V_prime=[V_prime, Anum((j-1)*n+1:j*n,:)*V_f];
          end
          V_f=orth(V_prime);
          quit = (r == rank(V_f));
      end
    
    
    Reach_mat = V_f;
    
    red_ord_r = r;
    
    Ar = zeros(D*red_ord_r,red_ord_r);
    
    V_inv=V_f';
    x0r=V_inv*x_0;

    
    Cr=C*V_f;

    for q=1:D
        Ar((q-1)*red_ord_r+1:q*red_ord_r,:)=V_inv*Anum((q-1)*n+1:q*n,:)*V_f;
    end


end