function testMinLPVCode(D,n,m,p,Anum,Bnum,Cnum,x0)


showLPV(D,n,m,p,Anum,Bnum,Cnum,x0)

disp('Check reachability')

[Reach_mat,r, Ar,Br,Cr,x0r]=ReachRedLPV(D,n,p,m,Anum,Bnum,Cnum,x0);


disp('Is reachable')
(r == n)

disp('Reachability transformation')
Reach_mat

disp('Result of reachability reduction')

showLPV(D,r,m,p,Ar,Br,Cr,x0r)


disp('Check observability')

[Obs_mat,ro, Ao,Bo,Co,x0o]=ObsRedLPV(D,n,m,p,Anum,Bnum,Cnum,x0);


disp('Is observable')
(ro == n)

disp('Observability transformation')
Obs_mat

disp('Result of observability reduction')

showLPV(D,ro,m,p,Ao,Bo,Co,x0o)


disp('Check minimality')

[rm, Am,Bm,Cm,x0m]=MinRedLPV(D,n,p,m,Anum,Bnum,Cnum,x0);


disp('Is minimal')
(rm == n)

disp('Result of minimality reduction')

showLPV(D,rm,m,p,Am,Bm,Cm,x0m)




function showLPV(Dl,nl,ml,pl,A,B,C,x0l)
  disp(['Number of linear subsystems: ',num2str(Dl),' number of states ', num2str(nl), 'number of outputs ', num2str(pl), 'number of inputs ', num2str(ml)]);
  
  for i=1:Dl
     disp(['A matrix for the ', num2str(i), 'th linear systems']);
     A((i-1)*nl+1:i*nl,:)
     
      disp(['B matrix for the ', num2str(i), 'th linear systems']);
      B(:,(i-1)*ml+1:i*ml)
      
     disp(['C matrix for the ', num2str(i), 'th linear systems']);
     C((i-1)*p+1:i*p,:)
  end    
  
  disp('Initial state');
  x0l
  
end 







end

