%This function checks if  a bilinear systme (C,A_1,...,A_D,x_0) 
%system is reachable.
%%Inputs: D=number of A matrices, n - number of continous states,
%p-number of outputs,
% Anum=[A1;A2;..;AD], C 
% x0 -- initial state
%The function returns true if the bilinear system is reachable and false
%otherwise.

function [IsReach]=IsReachBil(Anum,C,x0)
  [Reach_mat, red_ord_r,Ar,Cr,x0r]=ReachRedBil(Anum,C,x0);
  IsReach =(red_ord_r == size(Anum,2));
end