%This function performs minimality reduction of bilinear system (C,A_1,...,A_D,x_0). 
% Anum=[A1;A2;..;AD], C,   
% x0 -- initial state
%The matrices of the reduced system are stored in Am,Bm,Cm,x0m, according
%to the same principle as the input is stored; red_ord_m is the order of
%the minimal system.
function [red_ord_m,Am,Cm,x0m]=MinRedBil(Anum,Cnum,x_0)

  [Reach_mat, red_ord_r,Ar,Cr,x0r]=ReachRedBil(Anum,Cnum,x_0);
  
  [Wf,red_ord_m,Am,Cm,x0m]=ObsRedBil(Ar,Cr,x0r);
  

end

