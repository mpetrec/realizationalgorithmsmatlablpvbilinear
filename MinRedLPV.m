%This function performs minimality reduction of an ALPV system. 
%%Inputs: D=n_p, number of linear submodels, n - number of continous states,
%p-number of outputs.
% Anum=[A0;A1;A2;..;AD], Cnum=[C0;C1;..;CD] (rows one under the other),
% [B0,B1,...,BD] (columns stacked together).  
% x0 -- initial state
%The matrices of the reduced system are stored in Am,Bm,Cm,x0m, according
%to the same principle as the input is stored; red_ord_m is the order of
%the minimal system.
function [red_ord_m,Am,Bm,Cm,x0m]=MinRedLPV(D,n,m,p,Anum,Bnum,Cnum,x_0)

  [Reach_mat, red_ord_r,Ar,Br,Cr,x0r]=ReachRedLPV(D,n,m,p,Anum,Bnum,Cnum,x_0);
  
  [Wf,red_ord_m,Am,Bm,Cm,x0m]=ObsRedLPV(D,red_ord_r,m,p,Ar,Br,Cr,x0r)
  

end

