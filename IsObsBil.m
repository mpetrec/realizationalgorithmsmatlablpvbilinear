%This function checks if  a bilinear system (C,A_1,...,A_D,x_0) 
%system is observable.
% Anum=[A1;A2;..;AD], C 
% x0 -- initial state
%The function returns true if the bilinear system is observable and false
%otherwise.


function [IsObs]=IsObsBil(Anum,C,x0)
  [Wf,red_ord_m,Am,Cm,x0m]=ObsRedBil(Anum,C,x0);
  IsObs =(red_ord_m == size(Anum,2));
end