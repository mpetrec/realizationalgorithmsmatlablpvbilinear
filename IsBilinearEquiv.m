%This function returns 1 if the bilinear system (Cnum1,A1,...,AD,x01) and
%(Cnum2,B1,...,BD,x02) realize the same input-output map, where
% Anum1=[A1;A2;..;AD], Anum2=[B1;B2;...;BD]. The function compares the
% Markov parameters, so for large D, it tends to be slow. Try to use the
% function IsBilinearEquivFast for large D.

function [isEquiv] = IsBilinearEquiv(Anum1,Cnum1,x01, Anum2,Cnum2,x02)
epsilon = 1e-5;

n1 = size(Anum1,2);
n2 = size(Anum2,2);
D = size(Anum1,1)/n1

N = 2*max(n1,n2);

R=[x01;x02];
size(R);

compC = [Cnum1,-Cnum2];

isEquiv = 1;

counter = 0;
while (counter <= N & isEquiv==1)

    size(compC);
    size(R);
    
    diffMarkov = compC*R;
    
    isEquiv=(norm(diffMarkov,2) < epsilon);    
    
    Rnew = [];
    for i=1:D
     Rnew=[Rnew, blkdiag(Anum1((i-1)*n1+1:i*n1,:),Anum2((i-1)*n2+1:i*n2,:))*R];           
    end    
    R = Rnew;
    counter = counter + 1;
end    

end

